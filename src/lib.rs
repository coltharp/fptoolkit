//! Simple two-dimensional raster graphics.

use std::borrow::Borrow;
use std::cell::Cell;
use std::fmt::Debug;
use std::path::Path;
use std::rc::Rc;
use std::time::Duration;

use derivative::Derivative;
use image::{Rgba, RgbaImage};
use itertools::Itertools;
use noto_sans_mono_bitmap::FontWeight;
pub use noto_sans_mono_bitmap::RasterHeight as TextHeight;
use pixels::{Pixels, PixelsBuilder, SurfaceTexture};
use winit::dpi::{LogicalSize, Pixel, Size};
use winit::event::{MouseButton, WindowEvent};
use winit::event_loop::{EventLoop, EventLoopBuilder, EventLoopProxy};
pub use winit::keyboard::Key;
use winit::platform::pump_events::EventLoopExtPumpEvents;
use winit::window::{Window, WindowBuilder};
use winit_input_helper::WinitInputHelper;

/// A structure that can be used to dynamically receive a signal that some
/// condition has been met.
#[derive(Clone, Debug)]
struct Flag(Rc<Cell<bool>>);

impl Flag {
    /// Creates a new flag.  The flag begins in the "up" state.
    fn new() -> Flag {
        Flag(Rc::new(Cell::new(true)))
    }

    /// Sets the flag to the "down" state.
    fn set_down(&self) {
        self.0.set(false);
    }

    /// Queries whether the flag is "up".
    fn is_up(&self) -> bool {
        self.0.get()
    }
}

/// The type of winit events used in this module.
type Event = winit::event::Event<Flag>;

/// A handle for simple two-dimensional raster graphics.
///
/// # Drawing
///
/// A `Graphics` object ties together a *canvas* and a *window*.  To make
/// graphics happen, first you draw on the canvas, and then you display the
/// canvas on the window.
///
/// You can draw on the canvas with methods like [`pixel`](#pixel),
/// [`line`](#line), [`polygon`](#polygon), and [`fill_polygon`](#polygon).
/// Everything is drawn using the canvas's current drawing color, which you can
/// set via [`rgb`](#rgb).  (The canvas also has a "background color", but this
/// is only used for drawing text; you can set it via
/// [`background`](#background).)
///
/// When you want your changes to appear on the window, call
/// [`display_image`](#display-image).  You can also call an interaction
/// function; e.g., [`wait_click`](#wait_click) and [`wait_key`](#wait-key).
///
/// Note that the contents of the canvas persist between calls to
/// [`display_image`](#display-image).  If you want to redraw everything from
/// scratch, call [`clear`](#clear) to fill the canvas with the current drawing
/// color.
///
/// # Interaction
///
/// You can call [`wait_click`](#wait-click) to wait for a click, and you can
/// call [`wait_key`](#wait-key) to wait for a key press.
///
/// # Events
///
/// To keep the window responsive, the `Graphics` object needs to process window
/// events every so often.  [`display_image`](#display-image) automatically
/// processes all pending events, so if your application just draws in a loop,
/// you don't need to do anything special.  Methods that receive input from the
/// user also process events.  If your application has a long-running loop that
/// doesn't perform any graphics or interaction, you should call
/// [`events`](#events) in every iteration.
#[derive(Derivative)]
#[derivative(Debug)]
pub struct Graphics {
    width: u32,
    height: u32,
    event_loop: EventLoop<Flag>,
    proxy: EventLoopProxy<Flag>,
    window: Window,
    #[derivative(Debug = "ignore")]
    input: WinitInputHelper,
    pixels: Pixels,
    color: [u8; 4],
    background: [u8; 4],
}

impl Graphics {
    /// Creates a new `Graphics` object.  This automatically opens a window.
    /// The initial contents of the window are indeterminate, but the window
    /// starts out hidden; it will not appear on the screen until you render at
    /// least once.
    pub fn new(width: f64, height: f64) -> Graphics {
        let check = |d| assert!(d >= 0.0 && d < i32::MAX as f64, "invalid window dimensions");
        check(width);
        check(height);
        let event_loop = EventLoopBuilder::with_user_event()
            .build()
            .expect("building event loop failed");
        let proxy = event_loop.create_proxy();
        let window = WindowBuilder::new()
            .with_inner_size(Size::Logical(LogicalSize::new(width, height)))
            .with_visible(false)
            .build(&event_loop)
            .expect("building window failed");
        // XXX Fix weird macOS scaling behavior.
        let scale_factor = if cfg!(target_os = "macos") {
            window.scale_factor()
        } else {
            1.0
        };
        let input = WinitInputHelper::new();
        let surface = SurfaceTexture::new(
            (scale_factor * width).round() as u32,
            (scale_factor * height).round() as u32,
            &window,
        );
        let width = width.round() as u32;
        let height = height.round() as u32;
        let pixels = PixelsBuilder::new(width, height, surface)
            .build()
            .expect("building pixels failed");
        let color = [0xFF; 4];
        let background = [0x00, 0x00, 0x00, 0xFF];
        Graphics {
            width,
            height,
            event_loop,
            proxy,
            window,
            input,
            pixels,
            color,
            background,
        }
    }

    /// Renders the current contents of the canvas to the window.  Also
    /// processes all pending events.
    pub fn display_image(&mut self) {
        self.window.request_redraw();
        // We want to ensure that the redraw that we just enqueued is finished
        // before we return.  First, create a `Flag`.
        let flag = Flag::new();
        // Now send the flag to the event loop, which will set it down when it
        // processes it.
        //
        // XXX We assume that, since the redraw was requested before sending the
        // flag, the redraw request will be processed either before or in the
        // same batch of events as the flag.  As far as I can tell, `winit` does
        // not actually make such a guarantee, but it seems like any reasonable
        // implementation will work like this.
        self.proxy
            .send_event(flag.clone())
            .expect("sending event failed");
        while flag.is_up() {
            let wait = false;
            self.process_events(wait);
        }
    }

    /// Processes all pending events.
    pub fn events(&mut self) {
        let wait = false;
        self.process_events(wait);
    }

    /// Processes all pending events.  If the `wait` parameter is `true`, then
    /// this method blocks until there are events to process.
    fn process_events(&mut self, wait: bool) {
        let duration = if wait { None } else { Some(Duration::ZERO) };
        let mut events = Vec::new();
        self.event_loop
            .pump_events(duration, |event, _target| match event {
                Event::WindowEvent {
                    event: WindowEvent::RedrawRequested,
                    ..
                } => {
                    self.window.pre_present_notify();
                    self.pixels.render().expect("rendering failed");
                    // The window starts out invisible; it becomes visible after
                    // the first render.
                    self.window.set_visible(true);
                }
                Event::WindowEvent { event, .. } => {
                    events.push(event);
                }
                Event::UserEvent(flag) => {
                    flag.set_down();
                }
                _ => (),
            });
        self.input.step_with_window_events(&events);
        if self.input.close_requested() {
            std::process::exit(0);
        }
    }

    /// Sets the current drawing color.  All color components must be the range
    /// [0, 1].
    pub fn rgb(&mut self, red: f64, green: f64, blue: f64) {
        self.color = Self::color(red, green, blue);
    }

    /// Sets the background color for drawing; see [`rgb`](#rgb).  Currently,
    /// the background color is only used when drawing text.
    pub fn background(&mut self, red: f64, green: f64, blue: f64) {
        self.background = Self::color(red, green, blue);
    }

    /// Converts a triplet of color components to a single color value.
    fn color(red: f64, green: f64, blue: f64) -> [u8; 4] {
        let convert = |c: f64| {
            assert!(
                (0. ..=1.).contains(&c),
                "color components should be in the range [0, 1]"
            );
            (c * u8::MAX as f64).round() as u8
        };
        let red = convert(red);
        let green = convert(green);
        let blue = convert(blue);
        [red, green, blue, 0xFF]
    }

    /// Converts from spatial coordinates (`f64`, origin at bottom) to logical
    /// coordinates (`i32`, origin at top).
    fn spatial_to_logical(&self, (x, y): (f64, f64)) -> (i32, i32) {
        let x = x.round() as i32;
        let y = (self.height as f64 - 1. - y).round() as i32;
        (x, y)
    }

    /// Converts from physical coordinates (`f32`, origin at top) to spatial
    /// coordinates (`f64`, origin at bottom).
    fn physical_to_spatial(&self, (x, y): (f32, f32)) -> (f64, f64) {
        // First convert to logical coordinates.
        let scale_factor = self.window.scale_factor();
        let x = x.cast::<f64>() / scale_factor;
        let y = y.cast::<f64>() / scale_factor;
        // Now cast to spatial coordinates.
        let y = self.height as f64 - 1.0 - y;
        (x, y)
    }

    /// Draws a pixel at the given point.
    pub fn point(&mut self, point: (f64, f64)) {
        let point = self.spatial_to_logical(point);
        self.point_i(point);
    }

    /// Draws a pixel at the given point (in spatial coordinates).
    fn point_i(&mut self, point: (i32, i32)) {
        self.set_point_i(point, self.color);
    }

    /// Sets a pixel at the given point (in spatial coordinates) to the given
    /// color.
    fn set_point_i(&mut self, (x, y): (i32, i32), color: [u8; 4]) {
        if x < 0 || x >= self.width as i32 || y < 0 || y >= self.height as i32 {
            return;
        }
        let x = x as usize;
        let y = y as usize;
        self.pixels
            .frame_mut()
            .chunks_mut(4)
            .nth(y * self.width as usize + x)
            .unwrap()
            .copy_from_slice(&color);
    }

    /// Yields the "coordinates" of a Bresenham circle with the given center and
    /// radius (in physical coordinates).  Coordinates are yielded in the order
    /// `(cx, x, cy, y)`.
    fn circle_coordinates_i(
        (cx, cy): (i32, i32),
        radius: i32,
    ) -> impl Iterator<Item = (i32, i32, i32, i32)> {
        // Source:
        // https://www.computerenhance.com/p/efficient-dda-circle-outlines
        let mut x = radius;
        let mut y = 0;
        let mut dy = -2;
        let mut dx = 4 * (radius - 1);
        let mut d = 2 * radius - 1;
        std::iter::repeat_with(move || {
            if y > x {
                return None;
            }
            let ret = (cx, x, cy, y);
            d += dy;
            dy -= 4;
            y += 1;
            if d < 0 {
                d += dx;
                dx -= 4;
                x -= 1;
            }
            Some(ret)
        })
        .while_some()
    }

    /// Draws a circle with the given center and radius.
    pub fn circle(&mut self, center: (f64, f64), radius: f64) {
        let center = self.spatial_to_logical(center);
        let radius = radius.round() as i32;
        for (cx, x, cy, y) in Graphics::circle_coordinates_i(center, radius) {
            self.point_i((cx - x, cy - y));
            self.point_i((cx - x, cy + y));
            self.point_i((cx + x, cy - y));
            self.point_i((cx + x, cy + y));
            self.point_i((cx - y, cy - x));
            self.point_i((cx - y, cy + x));
            self.point_i((cx + y, cy - x));
            self.point_i((cx + y, cy + x));
        }
    }

    /// Draws a line between the given endpoints.
    pub fn line(&mut self, start: (f64, f64), end: (f64, f64)) {
        let start = self.spatial_to_logical(start);
        let end = self.spatial_to_logical(end);
        self.line_i(start, end);
    }

    /// Draws a line between the given endpoints (in spatial coordinates).
    fn line_i(&mut self, (x0, y0): (i32, i32), (x1, y1): (i32, i32)) {
        // Bresenham's algorithm.  Source: Wikipedia.
        //
        // This is a variant that detects the error dynamically in a way that
        // lets it works for all octants simultaneously.  You might think that
        // it would be more efficient to execute a different variant for each
        // octant with a single up-front check to determine which variant to
        // run; however, microbenchmarks suggest that that approach is actually
        // (slightly) *less* efficient.
        let dx = (x1 - x0).abs();
        let sx = if x0 < x1 { 1 } else { -1 };
        let dy = -(y1 - y0).abs();
        let sy = if y0 < y1 { 1 } else { -1 };

        let mut x = x0;
        let mut y = y0;
        let mut error = dx + dy;
        loop {
            self.point_i((x, y));
            if x == x1 && y == y1 {
                break;
            }
            let e2 = 2 * error;
            if e2 >= dy {
                if x == x1 {
                    break;
                }
                error += dy;
                x += sx;
            }
            if e2 <= dx {
                if y == y1 {
                    break;
                }
                error += dx;
                y += sy;
            }
        }
    }

    /// Draws a polygon with the given vertices.
    ///
    /// # Examples
    ///
    /// ```
    /// use fptoolkit::Graphics;
    /// let mut g = Graphics::new("example", 800, 800);
    /// g.polygon([(0., 0.), (400., 0.), (400., 400.), (0., 400.)]);
    /// ```
    pub fn polygon<U>(&mut self, points: U)
    where
        U: IntoIterator,
        U::Item: Borrow<(f64, f64)>,
        U::IntoIter: Clone + ExactSizeIterator,
    {
        for (start, end) in points
            .into_iter()
            .map(|x| *x.borrow())
            .circular_tuple_windows()
        {
            self.line(start, end);
        }
    }

    /// Draws a triangle with the given vertices.
    pub fn triangle(&mut self, a: (f64, f64), b: (f64, f64), c: (f64, f64)) {
        self.polygon([a, b, c]);
    }

    /// Draws a rectangle with the given lower-left corner and dimensions.
    pub fn rectangle(&mut self, (x, y): (f64, f64), width: f64, height: f64) {
        self.polygon([
            (x, y),
            (x + width, y),
            (x + width, y + height),
            (x, y + height),
        ]);
    }

    /// Fills the canvas with the current drawing color.
    pub fn clear(&mut self) {
        for pixel in self.pixels.frame_mut().chunks_mut(4) {
            pixel.copy_from_slice(&self.color);
        }
    }

    /// Fills a circle with the given center and radius.
    pub fn fill_circle(&mut self, center: (f64, f64), radius: f64) {
        let center = self.spatial_to_logical(center);
        let radius = radius.round() as i32;
        for (cx, x, cy, y) in Graphics::circle_coordinates_i(center, radius) {
            self.line_i((cx - x, cy - y), (cx + x, cy - y));
            self.line_i((cx - x, cy + y), (cx + x, cy + y));
            self.line_i((cx - y, cy - x), (cx + y, cy - x));
            self.line_i((cx - y, cy + x), (cx + y, cy + x));
        }
    }

    /// Fills a polygon with the given vertices.
    ///
    /// # Examples
    ///
    /// ```
    /// use fptoolkit::Graphics;
    /// let mut g = Graphics::new("example", 800, 800);
    /// g.fill_polygon([(0., 0.), (400., 0.), (400., 400.), (0., 400.)]);
    /// ```
    pub fn fill_polygon<U>(&mut self, points: U)
    where
        U: IntoIterator,
        U::Item: Borrow<(f64, f64)>,
        U::IntoIter: Clone + ExactSizeIterator,
    {
        // Sources:
        //
        // "A New Polygon Based Algorithm for Filling Regions"
        //
        // (It’s about a different algorithm, but it mentions this one for
        // comparison.)
        //
        // https://www.cs.uic.edu/~jbell/CourseNotes/ComputerGraphics/PolygonFilling.html

        const SIZE_ERROR_MSG: &str =
            "fill_polygon got fewer points than expected (is your ExactSizeIterator bugged?)";

        let mut points = points
            .into_iter()
            .map(|x| self.spatial_to_logical(*x.borrow()));
        let num_points = points.len();

        match num_points {
            0 => {
                return;
            }
            1 => {
                self.point_i(points.next().expect(SIZE_ERROR_MSG));
                return;
            }
            2 => {
                self.line_i(
                    points.next().expect(SIZE_ERROR_MSG),
                    points.next().expect(SIZE_ERROR_MSG),
                );
                return;
            }
            _ => (),
        }

        let (y_min, y_max) = points
            .clone()
            .map(|p| p.1)
            .minmax()
            .into_option()
            .expect(SIZE_ERROR_MSG);

        // `edge_groups` will be a list of edge groups, each group consisting of
        // edges that have the same initial (low) y-value, sorted in order of
        // initial y-value.
        //
        // (In fact, it will be sorted in *reverse* order; i.e., the lowest
        // y-value will come last.  The reason for this is that we're going to
        // pop groups off the end one-by-one, and we want to go in order of low
        // y-value.)
        let mut edge_groups = points
            // Iterate over pairs of adjacent points; i.e., edges.
            .circular_tuple_windows()
            // Remove horizontal edges.
            .filter(|(p, q)| p.1 != q.1)
            // Make sure the point with the low y-value always occurs first.
            .map(|(p, q)| if p.1 < q.1 { (p, q) } else { (q, p) })
            // Sort and group edges by low y-value.
            .sorted_unstable_by(|p, q| q.0 .1.cmp(&p.0 .1))
            .group_by(|p| p.0 .1)
            .into_iter()
            // For each group of lines with the same initial (low) y-value:
            .map(|(y, group)| {
                (
                    y,
                    group
                        // Compute the slope of each line.
                        .map(|((x0, y0), (x1, y1))| {
                            (y1, x0, ((x1 - x0) as f64) / ((y1 - y0) as f64))
                        })
                        // Sort by x-value.
                        .sorted_unstable_by_key(|u| u.1)
                        .map(|(y, x, m)| (y, x as f64, m)),
                )
            })
            .collect_vec();

        let mut active = Vec::<(i32, f64, f64)>::new();
        active.reserve_exact(num_points);
        for y in y_min..=y_max {
            // Drop any edge whose final y-value lies on the current scanline.
            active.retain(|u| u.0 != y);
            // If the initial y-value of the next edge group is equal to the
            // y-value of the current scanline, pop that group and add its edges
            // to the active set.
            if edge_groups.last().map_or(false, |g| g.0 == y) {
                active.extend(edge_groups.pop().unwrap().1);
            }
            // Sort edges by x-value.
            active.sort_unstable_by(|u, v| u.1.partial_cmp(&v.1).unwrap());
            // Draw scanlines using the parity rule.
            for (u, v) in active.iter().tuple_windows().step_by(2) {
                let x0 = u.1.round() as i32;
                let x1 = v.1.round() as i32;
                for x in x0..=x1 {
                    self.point_i((x, y));
                }
            }
            // Increase the x-value of each active edge by its slope.
            for u in &mut active {
                u.1 += u.2;
            }
        }
    }

    /// Fills a triangle with the given vertices.
    pub fn fill_triangle(&mut self, a: (f64, f64), b: (f64, f64), c: (f64, f64)) {
        self.fill_polygon([a, b, c]);
    }

    /// Fills a rectangle with the given lower-left corner and dimensions.
    pub fn fill_rectangle(&mut self, (x, y): (f64, f64), width: f64, height: f64) {
        self.fill_polygon([
            (x, y),
            (x + width, y),
            (x + width, y + height),
            (x, y + height),
        ]);
    }

    /// Draws a string.
    pub fn draw_string(&mut self, string: &str, height: TextHeight, (x, y): (f64, f64)) {
        let (mut x, y) = self.spatial_to_logical((x, y + height.val() as f64));
        let [red, green, blue, _] = self.color.map(|c| c as f64);
        let [bg_red, bg_green, bg_blue, _] = self.background.map(|c| c as f64);
        for c in string.chars() {
            let raster = noto_sans_mono_bitmap::get_raster(c, FontWeight::Regular, height)
                .expect("invalid character");
            for (i, row) in raster.raster().iter().enumerate() {
                for (j, &lum) in row.iter().enumerate() {
                    let lum = lum as f64;
                    let lum_cmp = u8::MAX as f64 - lum;
                    let red = (lum * red + lum_cmp * bg_red).round() as u8;
                    let green = (lum * green + lum_cmp * bg_green).round() as u8;
                    let blue = (lum * blue + lum_cmp * bg_blue).round() as u8;
                    let color = [red, green, blue, 0xFF];
                    self.set_point_i((x + j as i32, y + i as i32), color);
                }
            }
            x += raster.width() as i32;
        }
    }

    /// Saves the current contents of the canvas to an image file.  The format
    /// is determined by the file extension.
    pub fn save_image_to_file<P>(&self, filename: P)
    where
        P: AsRef<Path>,
    {
        let buf = self.pixels.frame().to_vec();
        let image = RgbaImage::from_vec(self.width, self.height, buf).unwrap();
        image.save(filename).expect("saving image failed");
    }

    /// Reads an image from a file and draws it on the canvas.  The image is
    /// drawn with its lower left corner at the given coordinate.
    pub fn get_image_from_file<P>(&mut self, filename: P, (x, y): (f64, f64))
    where
        P: AsRef<Path>,
    {
        let image = image::io::Reader::open(filename)
            .expect("opening image file failed")
            .decode()
            .expect("decoding image failed")
            .into_rgba8();
        let height = image.height() as f64;
        let (x, y) = self.spatial_to_logical((x, y + height));
        for (i, j, &Rgba(pixel)) in image.enumerate_pixels() {
            self.set_point_i((x + i as i32, y + j as i32), pixel);
        }
    }

    /// Waits for a click in the window; returns the clicked coordinate.  Also
    /// immediately renders the canvas to the window.
    pub fn wait_click(&mut self) -> (f64, f64) {
        self.display_image();
        loop {
            let wait = true;
            self.process_events(wait);
            if self.input.mouse_released(MouseButton::Left) {
                let point = self.input.cursor().unwrap();
                break self.physical_to_spatial(point);
            }
        }
    }

    /// Waits for a key press; returns the keys that were pressed.  Also
    /// immediately renders the canvas to the window.
    pub fn wait_key(&mut self) -> Vec<Key> {
        self.display_image();
        loop {
            let wait = true;
            self.process_events(wait);
            let text = self.input.text();
            if !text.is_empty() {
                break text.to_vec();
            }
        }
    }
}

{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-parts.url = "github:hercules-ci/flake-parts";
    rust-overlay.url = "github:oxalica/rust-overlay";
    rust-overlay.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = inputs@{ flake-parts, ... }: flake-parts.lib.mkFlake { inherit inputs; } {
    systems = [ "aarch64-darwin" "aarch64-linux" "x86_64-darwin" "x86_64-linux" ];
    perSystem = { system, pkgs, ... }: {
      _module.args.pkgs = import inputs.nixpkgs {
        inherit system;
        overlays = [ inputs.rust-overlay.overlays.default ];
      };
      devShells.default = pkgs.mkShell {
        packages = [
          (pkgs.rust-bin.stable.latest.default.override {
            extensions = [ "rust-analyzer" ];
          })
        ] ++ (if pkgs.stdenv.isDarwin then [
          pkgs.darwin.apple_sdk.frameworks.AppKit
          pkgs.darwin.apple_sdk.frameworks.QuartzCore
        ] else [ ]);
        LD_LIBRARY_PATH = pkgs.lib.concatStringsSep ":" (
          if pkgs.stdenv.isLinux then [
            "${pkgs.xorg.libX11}/lib"
            "${pkgs.xorg.libXcursor}/lib"
            "${pkgs.xorg.libxcb}/lib"
            "${pkgs.xorg.libXi}/lib"
            "${pkgs.libxkbcommon}/lib"
            "${pkgs.wayland}/lib"
            "${pkgs.vulkan-loader}/lib"
          ] else [ ]
        );
      };
    };
  };
}

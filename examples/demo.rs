use fptoolkit::Graphics;

fn main() {
    let swidth = 400.0;
    let sheight = 600.0;
    let mut g = Graphics::new(swidth, sheight);

    // Clear the screen in a given color.
    g.rgb(0.3, 0.3, 0.3);  // Dark gray.
    g.clear();

    // Draw a point.
    g.rgb(1.0, 0.0, 0.0);  // Red.
    g.point((200.0, 580.0));

    // Draw a line.
    g.rgb(0.0, 1.0, 0.0);  // Green.
    g.line((0.0, 0.0), (swidth - 1.0, sheight - 1.0));

    // Aligned rectangles.
    g.rgb(0.0, 0.0, 1.0);  // Blue.
    let lowleftx = 200.0;
    let lowlefty = 50.0;
    let width = 10.0;
    let height = 30.0;
    g.rectangle((lowleftx, lowlefty), width, height);
    let lowleftx = 250.0;
    g.fill_rectangle((lowleftx, lowlefty), width, height);

    // Triangles.
    g.rgb(1.0, 1.0, 0.0);  // Yellow.
    g.triangle((10.0, 300.0), (40.0, 300.0), (60.0, 250.0));
    g.fill_triangle((10.0, 100.0), (40.0, 100.0), (60.0, 150.0));

    // Circles.
    g.rgb(1.0, 0.5, 0.0);  // Orange.
    g.circle((100.0, 300.0), 75.0);
    g.fill_circle((370.0, 200.0), 50.0);

    // Polygons.
    g.rgb(0.0, 0.0, 0.0);  // Black.
    let xy = [
        (100.0, 100.0),
        (100.0, 300.0),
        (300.0, 300.0),
        (300.0, 100.0),
        (200.0, 175.0),
    ];
    g.polygon(xy);

    g.rgb(0.4, 0.2, 0.1);  // Brown.
    let ab = [
        (300.0, 400.0),
        (350.0, 450.0),
        (275.0, 500.0),
        (125.0, 400.0),
    ];
    g.fill_polygon(ab);

    g.rgb(1.0, 0.0, 0.0);
    let p = g.wait_click();
    g.fill_circle(p, 2.0);

    let q = g.wait_click();
    g.fill_circle(q, 2.0);

    g.rgb(0.0, 1.0, 0.5);
    g.line(p, q);

    g.wait_key();
}

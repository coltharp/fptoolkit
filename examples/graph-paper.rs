use std::io::{self, Write};

use fptoolkit::Graphics;

const DIM: f64 = 600.0;

fn main() {
    print!("Grid dimension: ");
    io::stdout().flush().unwrap();
    let num_lines = {
        let mut line = String::new();
        io::stdin().read_line(&mut line).unwrap();
        let line = line.trim_end();
        u32::from_str_radix(&line, 10).expect("invalid input")
    };

    let mut g = Graphics::new(DIM, DIM);
    g.rgb(1.0, 1.0, 1.0);
    g.clear();
    g.rgb(0.0, 0.0, 0.0);
    for i in 0..num_lines {
        let coord = i as f64 * (DIM / num_lines as f64);
        g.line((0.0, coord), (DIM - 1.0, coord));
        g.line((coord, 0.0), (coord, DIM - 1.0));
    }
    g.wait_key();
}
